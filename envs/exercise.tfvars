environment = "exercise"

dns_zone = "spos2.sgfl.xyz."
region   = "us-west1"
zone     = "us-west1-a"

linux_instances  = []
linux_disk_datas = []

windows_instances  = []
windows_disk_datas = []

manage_ssh_keys = true
