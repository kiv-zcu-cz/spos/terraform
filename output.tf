output "dns_zone" {
  value = var.dns_zone
}
output "name_servers" {
  value = google_dns_managed_zone.base.name_servers
}

output "windows_instances" {
  value = [for server in var.windows_instances : { instance = server, name = module.windows[server].name, public_ip = module.windows[server].public_ip, dns = google_dns_record_set.windows[server].name }]
}

output "linux_instances" {
  value = [for server in var.linux_instances : { instance = server, name = module.linux[server].name, public_ip = module.linux[server].public_ip, dns = google_dns_record_set.linux[server].name }]
}