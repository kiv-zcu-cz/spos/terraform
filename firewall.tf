locals {
  firewall_rules = {
    openvpn-server = {
      tcp = ["1194"]
      udp = ["1194"]
    }
    http-server = {
      tcp = ["80"]
    }
    https-server = {
      tcp = ["443"]
    }
    http-alt-server = {
      tcp = ["8080"]
    }
    https-alt-server = {
      tcp = ["8443"]
    }
  }
}

resource "google_compute_firewall" "rules" {
  for_each    = local.firewall_rules
  project     = var.project
  name        = "${each.key}-allow-rule"
  network     = "default"
  description = "Creates firewall rule for ${each.key} tagged instances"

  dynamic "allow" {
    for_each = each.value
    content {
      protocol = allow.key
      ports    = allow.value
    }
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = [each.key]
}
