resource "google_compute_project_metadata" "metadata" {
  count   = var.manage_ssh_keys ? 1 : 0
  project = var.project
  metadata = {
    "ssh-keys" = <<-EOT
            jindrich.skupa:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTCXxZ0kcgXZ9pHx0RnBNsdyM6ajCPCebew4dHfJOtSmcRYuZnKXRqdhW+usGC9itdxx3hxZr7JaCE4rJjjp6pS+EvggG8ZIaSB6EeCyTqkWgmahODnjOEz8sBRdLIlrR8VpnoB1nAz4/GZXKo6K7HlyLLyDTOfcGQiigW0bvrSKi2YfdlekxJalMpHdsX1Hi5SSIUSPOAykD7FHVWt2Q18ctItOTrXLcKXxYDp6rXH+zmD/u+fKg/H8wed+AbWFVYFlEB6e0qssuzpqm6NeHMAlQELnJZUiFieanZq+D5H9Tp6jOxjBUA3au3rP9wJBTCYFiDIBiuG+0RkX2zk7PR jindrich.skupa@gmail.com
            jindrich.skupa:ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJDFRcIrAV4pXafNkXIu81Ouq1AwLFb9+o9NBzqaW0QU jindrich.skupa@gmail.com
            spos:ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBL4ZAk4CHSBWerlz+C9rCnbnkbTcy3YBW9d4uQMQ2KvIS9bb2a/vUkRMoklo5L49wLBFgEQsmoOuj/ivsXpIKAU= spos@kiv.zcu.cz
        EOT
  }
}