module "linux" {
  for_each = toset(var.linux_instances)
  source   = "./modules/machine"

  name = each.key

  project      = var.project
  project_name = "${var.project_name}-lin"
  environment  = var.environment
  region       = var.region
  zone         = var.zone

  disk_data_size   = var.linux_disk_data_size
  disk_datas       = var.linux_disk_datas
  disk_system_size = var.linux_disk_system_size
  instance_size    = var.linux_instance_size
  instance_image   = "debian-cloud/debian-11"
}

module "windows" {
  for_each = toset(var.windows_instances)
  source   = "./modules/machine"

  name = each.key

  project      = var.project
  project_name = "${var.project_name}-win"
  environment  = var.environment
  region       = var.region
  zone         = var.zone

  disk_data_size   = var.windows_disk_data_size
  disk_datas       = var.windows_disk_datas
  disk_system_size = var.windows_disk_system_size
  instance_size    = var.windows_instance_size
  instance_image   = "windows-cloud/windows-2019"
}