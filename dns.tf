locals {
  domain_slug = replace(replace(replace(lower(var.dns_zone), "/\\W|_|\\s/", "-"), "/-+/", "-"), "/-$/", "")
}
resource "google_dns_managed_zone" "base" {
  name        = "dns-${local.domain_slug}"
  dns_name    = var.dns_zone
  description = "KIV/SPOS sgfl.xyz DNS zone"
  labels = {
    domain = "sgfl"
  }
}

resource "google_dns_record_set" "linux" {
  for_each = toset(var.linux_instances)
  name     = "${each.key}-lin-${var.environment}.${google_dns_managed_zone.base.dns_name}"
  type     = "A"
  ttl      = 300

  managed_zone = google_dns_managed_zone.base.name

  rrdatas = [module.linux[each.key].public_ip]
}

resource "google_dns_record_set" "windows" {
  for_each = toset(var.windows_instances)
  name     = "${each.key}-win-${var.environment}.${google_dns_managed_zone.base.dns_name}"
  type     = "A"
  ttl      = 300

  managed_zone = google_dns_managed_zone.base.name

  rrdatas = [module.windows[each.key].public_ip]
}