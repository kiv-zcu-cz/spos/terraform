variable "project" {
  type    = string
  default = "skupaj-kiv-spos"
}

variable "project_name" {
  type    = string
  default = "spos"
}

variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-a"
}

variable "environment" {
  type = string
}
variable "linux_disk_data_size" {
  type    = number
  default = 10
}

variable "linux_disk_datas" {
  type    = list(string)
  default = []
}

variable "linux_disk_system_size" {
  type    = number
  default = 10
}

variable "linux_instance_size" {
  type    = string
  default = "e2-small"
}

variable "linux_instances" {
  type    = list(string)
  default = []
}

variable "windows_disk_data_size" {
  type    = number
  default = 10
}

variable "windows_disk_datas" {
  type    = list(string)
  default = []
}

variable "windows_disk_system_size" {
  type    = number
  default = 100
}

variable "windows_instance_size" {
  type    = string
  default = "e2-standard-4"
}

variable "windows_instances" {
  type    = list(string)
  default = []
}

variable "dns_zone" {
  type    = string
  default = "spos.sgf.xyz."
}

variable "manage_ssh_keys" {
  type    = bool
  default = false
}